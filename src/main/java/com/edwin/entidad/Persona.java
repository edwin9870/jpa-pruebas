package com.edwin.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "PERSONA")
@NamedQueries({
	@NamedQuery(name = "Persona.buscarTodos", query = "SELECT a FROM Persona a"),
	@NamedQuery(name = "Persona.buscarPorNombre", query = "SELECT a FROM Persona a WHERE a.nomber = :nombre")
})
public class Persona {
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "NOMBER")
	private String nomber;
	@Column(name = "APELLIDO")
	private String apellido;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNomber() {
		return nomber;
	}
	public void setNomber(String nomber) {
		this.nomber = nomber;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	@Override
	public String toString() {
		return "Persona [id=" + id + ", nomber=" + nomber + ", apellido=" + apellido + "]";
	}
	
}