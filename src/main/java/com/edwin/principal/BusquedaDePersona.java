package com.edwin.principal;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.edwin.entidad.Persona;

public class BusquedaDePersona {

	public static void main(String[] args) {
		//Persona.buscarPorNombre
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("klk");
		EntityManager em = emf.createEntityManager();
		
		Persona persona = em.createNamedQuery("Persona.buscarPorNombre", Persona.class).setParameter("nombre", "Gonzalo").getSingleResult();
		
		System.out.println("Persona encontrada: " + persona);
	}

}
