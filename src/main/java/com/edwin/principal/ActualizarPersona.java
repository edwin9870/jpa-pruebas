package com.edwin.principal;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.edwin.entidad.Persona;

public class ActualizarPersona {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("klk");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();
		Persona cindy = em.find(Persona.class, 1);
		System.out.println("Actualizando persona: "+ cindy);
		
		cindy.setApellido("Lol");
		
		System.out.println("Persona con los datos actualizado: " + cindy);
		em.getTransaction().commit();
		
		System.out.println("Persona actualizada");

	}

}
