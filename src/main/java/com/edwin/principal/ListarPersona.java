package com.edwin.principal;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.edwin.entidad.Persona;

public class ListarPersona {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("klk");
		EntityManager em = emf.createEntityManager();

		List<Persona> resultList = em.createNamedQuery("Persona.buscarTodos", Persona.class).getResultList();
		System.out.println(resultList);

	}
}
