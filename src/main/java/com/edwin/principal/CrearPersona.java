package com.edwin.principal;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.edwin.entidad.Persona;

public class CrearPersona {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("klk");
		EntityManager em = emf.createEntityManager();

		Persona persona = new Persona();
		persona.setNomber("Gonzalo");
		persona.setApellido("Rodriguez");
		
		System.out.println("Persona a guardar: " + persona);
		
		em.getTransaction().begin();
		em.persist(persona);
		em.getTransaction().commit();
		System.out.println("Persona guardada correctamente");
		

	}

}
